package com.example.yanghancheng.eptenderzentri;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

public class TestFrameActivity extends BaseActivity {

    private SwitchCompat modeSwitch;
    private SwitchCompat engineSwitch;
    private SwitchCompat joWhSwitch;
    private SwitchCompat basculeSwitch;
    private TextView basculeText;
    private TextView thtlText;
    private SeekBar thtlBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_frame);
        modeSwitch = (SwitchCompat)findViewById(R.id.ManuelModeSwith);
        engineSwitch = (SwitchCompat)findViewById(R.id.EngineSwitch);
        engineSwitch.setEnabled(false);
        joWhSwitch = (SwitchCompat)findViewById(R.id.JoWhSwitch);
        joWhSwitch.setEnabled(false);
        basculeSwitch = (SwitchCompat)findViewById(R.id.BasculeSwitch);
        basculeText = (TextView)findViewById(R.id.BasculeText);
        basculeSwitch.setEnabled(false);
        thtlText = (TextView)findViewById(R.id.ThrottleText);
        thtlBar = (SeekBar)findViewById(R.id.ThtlSeekbar);
        thtlBar.setEnabled(false);
        modeSwitch.setOnClickListener(Listner);
        engineSwitch.setOnClickListener(Listner);
        joWhSwitch.setOnClickListener(Listner);
        basculeSwitch.setOnClickListener(Listner);
        thtlBar.setOnSeekBarChangeListener(barListner);
    }

    @Override
    protected void afterServiceConnected() {

    }

    @Override
    protected void afterBroadcastReceive(String action, Intent intent) {

    }

    View.OnClickListener Listner = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.ManuelModeSwith:
                    if(modeSwitch.isChecked()){
                        engineSwitch.setEnabled(true);
                        joWhSwitch.setEnabled(true);
                        basculeSwitch.setEnabled(true);
                        thtlBar.setEnabled(true);
                    } else {
                        engineSwitch.setEnabled(false);
                        joWhSwitch.setEnabled(false);
                        basculeSwitch.setEnabled(false);
                        thtlBar.setEnabled(false);
                    }
                    break;
                case R.id.BasculeSwitch:
                    basculeText.setText(basculeSwitch.isChecked()?"Parallel":"Serial");
            }
            SendTestFrame();
        }
    };

    SeekBar.OnSeekBarChangeListener barListner = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            thtlText.setText(Integer.toString(progress) + " %");
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            SendTestFrame();
        }
    };


    void SendTestFrame(){
        String mode = modeSwitch.isChecked() ? "1":"0";
        String engine = engineSwitch.isChecked() ? "1":"0";
        String jowh = joWhSwitch.isChecked() ? "1":"0";
        String bascule = basculeSwitch.isChecked() ? "2":"1";
        String thtl = Integer.toString(thtlBar.getProgress());
        mZentriOSBLEManager.writeData("{3:"+bascule+":"+thtl+":"+jowh+":"+engine+":"+mode+"}");
    }
}
