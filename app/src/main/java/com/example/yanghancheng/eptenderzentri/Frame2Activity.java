package com.example.yanghancheng.eptenderzentri;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class Frame2Activity extends BaseActivity {

    private TCUFrame mTCUFrame=null;
    private String SOC, targetSOC, runDistance, fuelLevel, temp, power;
    private TextView infoTextView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_frame2);
        infoTextView = (TextView)findViewById(R.id.infoTextView);
        mTCUFrame = new TCUFrame("2","SOC","targetSOC","runDistance","fuelLevel","temp","power");
    }

    @Override
    protected void afterServiceConnected() {
        mZentriOSBLEService.setRequestID("2");
    }

    @Override
    protected void afterBroadcastReceive(String action, Intent intent) {
        switch (action){
            case ZentriOSBLEService.ACTION_STRING_DATA_READ:
                String buffer =ZentriOSBLEService.getData(intent);
                if(mTCUFrame.load(buffer)) {
                    targetSOC = (String)mTCUFrame.get("targetSOC");
                    SOC = (String) mTCUFrame.get("SOC");
                    fuelLevel = (String) mTCUFrame.get("fuelLevel");
                    runDistance = (String)mTCUFrame.get("runDistance");
                    temp = (String)mTCUFrame.get("temp");
                    power = (String)mTCUFrame.get("power");
                    infoTextView.setText("Target SOC : \t" + targetSOC
                                        +"\nSOC : \t"+ SOC
                                        +"\nFuel Level : \t" + fuelLevel
                                        +"\nCovered Distance : \t" + runDistance
                                        +"\nEngine Temperature : \t" + temp
                                        +"\nPower : \t" + power);
                }
                break;
        }
    }

}
