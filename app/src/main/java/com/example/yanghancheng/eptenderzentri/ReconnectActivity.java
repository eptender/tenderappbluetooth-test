package com.example.yanghancheng.eptenderzentri;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class ReconnectActivity extends BaseActivity {

    /** Request code to distinguish different **/
    private static final int BLE_ENABLE_REQ_CODE = 1;

    private ProgressDialog m_pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        m_pDialog = new ProgressDialog(this);
        m_pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        m_pDialog.setMessage("Connection lost\n Try to reconnect");
        m_pDialog.setIndeterminate(false);
        m_pDialog.setCancelable(false);
        m_pDialog.show();
    }

    @Override
    protected void afterServiceConnected() {
        if(requirementsMet()){
            startScan();
        }
    }

    @Override
    protected void afterBroadcastReceive(String action, Intent intent) {
        switch (action) {
            case ZentriOSBLEService.ACTION_SCAN_RESULT:
                //connect the 1st device whose name matches
                String name = ZentriOSBLEService.getData(intent);
                if (name!=null && name.startsWith(Const.DEVICE_NAME) && !mConnecting && !mConnected) {
                    mZentriOSBLEManager.stopScan();
                    mZentriOSBLEManager.connect(name);
                    mConnecting = true;
                }
                break;
            case ZentriOSBLEService.ACTION_CONNECTED:
                mConnected = true;
                Log.i(TAG, "Connected to " + ZentriOSBLEService.getData(intent));

                m_pDialog.hide();
                finish();
                break;
        }
    }

    /** Utility method **/
    private boolean requirementsMet(){
        boolean reqMet = false;
        if(!mZentriOSBLEManager.isInitialised()){
            //Enable BT
            startBLEEnableIntent();
        } else {
            reqMet = true;
        }
        return reqMet;
    }

    private void startBLEEnableIntent() {
        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivityForResult(enableBtIntent, BLE_ENABLE_REQ_CODE);
    }

    /** Activity with result **/
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == BLE_ENABLE_REQ_CODE){
            mZentriOSBLEService.initTruconnectManager();
            if(mZentriOSBLEManager.isInitialised()){
                //2nd try to initialise
                if(requirementsMet()){
                    startScan();
                } else {
                    // totally failed
                    showErrorDialog(R.string.init_fail_msg, true);
                }
            }
        }
    }

    /** Zentri function **/
    public void startScan(){
        if(mZentriOSBLEManager != null){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mZentriOSBLEManager.startScan();
                }
            });
        }
    }


}
