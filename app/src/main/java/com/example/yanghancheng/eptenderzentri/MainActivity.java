package com.example.yanghancheng.eptenderzentri;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ToggleButton;

public class MainActivity extends BaseActivity {

    private TextView rxTextView;
    private EditText targetDistanceText;
    private EditText targetSOCText;
    private Button EOLButton;
    private ToggleButton greenModeToggle;
    private Button updateConfigButton;
    private EditText txTextView;
    private Button sendButton;
    private Button frame2Button;
    private Button frame3Button;
    private Button frame4Button;
    private Button toTestButton;
    private TCUFrame mTCUFrame=null;
    private String SOC,fuelLevel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        targetDistanceText = (EditText)findViewById(R.id.TargetDistanceText);
        targetSOCText = (EditText)findViewById(R.id.TargetSOCText);
        EOLButton = (Button)findViewById(R.id.EOLButton);
        greenModeToggle = (ToggleButton)findViewById(R.id.GreenModeToggleButton);
        updateConfigButton = (Button)findViewById(R.id.UpdateConfigButton);
        rxTextView = (TextView)findViewById(R.id.RxTextView);
        txTextView = (EditText)findViewById(R.id.TxTextView);
        sendButton = (Button)findViewById(R.id.SendButtonView);
        frame2Button = (Button)findViewById(R.id.ToFrame2ButtonView);
        frame3Button = (Button)findViewById(R.id.ToFrame3ButtonView);
        frame4Button = (Button)findViewById(R.id.ToFrame4ButtonView);
        toTestButton = (Button)findViewById(R.id.ToTestButtonView);
        EOLButton.setOnClickListener(EOLButtonOnClickListner);
        greenModeToggle.setOnClickListener(GreenModeButtonOnClickListner);
        updateConfigButton.setOnClickListener(UpdateConfigButtonOnClickListner);
        sendButton.setOnClickListener(sendButtonOnClickListner);
        frame2Button.setOnClickListener(frame2ButtonOnClickListner);
        frame3Button.setOnClickListener(frame3ButtonOnClickListner);
        frame4Button.setOnClickListener(frame4ButtonOnClickListner);
        toTestButton.setOnClickListener(toTestButtonOnClickListner);
        /** Initialise frame here**/
        mTCUFrame = new TCUFrame("1","SOC","FuelLevel");
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    @Override
    protected void afterServiceConnected() {
        mZentriOSBLEService.setRequestID("1");
    }

    @Override
    protected void afterBroadcastReceive(String action, Intent intent) {
        switch (action){
            case ZentriOSBLEService.ACTION_STRING_DATA_READ:
                String buffer =ZentriOSBLEService.getData(intent);
                if(mTCUFrame.load(buffer)) {
                    SOC = (String) mTCUFrame.get("SOC");
                    fuelLevel = (String) mTCUFrame.get("FuelLevel");
                    rxTextView.setText("SOC=" + SOC + "\nFuel Level=" + fuelLevel);
                }
                break;
        }
    }

    View.OnClickListener sendButtonOnClickListner = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(mZentriOSBLEManager!=null && txTextView.getText()!=null){
                mZentriOSBLEManager.writeData(txTextView.getText().toString());
            }
        }
    };

    View.OnClickListener frame2ButtonOnClickListner = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(MainActivity.this, Frame2Activity.class);
            MainActivity.this.startActivity(intent);
        }
    };

    View.OnClickListener frame3ButtonOnClickListner = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(MainActivity.this, Frame3Activity.class);
            MainActivity.this.startActivity(intent);
        }
    };

    View.OnClickListener frame4ButtonOnClickListner = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(MainActivity.this, Frame4Activity.class);
            MainActivity.this.startActivity(intent);
        }
    };

    View.OnClickListener toTestButtonOnClickListner = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(MainActivity.this, TestFrameActivity.class);
            MainActivity.this.startActivity(intent);
        }
    };

    View.OnClickListener EOLButtonOnClickListner = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mZentriOSBLEManager.writeData("{2:1:"+(greenModeToggle.isChecked()?"1}":"0}"));
        }
    };

    View.OnClickListener GreenModeButtonOnClickListner = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mZentriOSBLEManager.writeData("{2:0:"+(greenModeToggle.isChecked()?"1}":"0}"));
        }
    };

    View.OnClickListener UpdateConfigButtonOnClickListner = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(targetSOCText.getText()!=null&&targetDistanceText.getText()!=null)
                mZentriOSBLEManager.writeData("{1:"+targetDistanceText.getText().toString()+":"+
                                            targetSOCText.getText().toString()+"}");
        }
    };
}