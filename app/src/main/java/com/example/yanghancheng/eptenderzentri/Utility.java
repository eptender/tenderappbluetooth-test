package com.example.yanghancheng.eptenderzentri;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Build;

/**
 * Created by YANG Hancheng on 18/05/2016.
 */
public class Utility {

    public static Dialog showErrorDialog(final Activity activity, final int titleID,
                                         final int msgID, final boolean finishOnClose){
        Dialog dialog = null;
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        if(!activity.isFinishing()){
            dialog = builder.setCancelable(false)
                     .setTitle(titleID)
                     .setMessage(msgID)
                     .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                         @Override
                         public void onClick(DialogInterface dialog, int which) {
                             dialog.dismiss();
                             if(finishOnClose){
                                 activity.finish();
                             }
                         }
                     }).create();
            dialog.show();
        }
        return dialog;
    }

    static boolean isPreMarshmallow(){
        return (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP_MR1);
    }

}
