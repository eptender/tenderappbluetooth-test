package com.example.yanghancheng.eptenderzentri;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Frame3Activity extends BaseActivity {

    private TCUFrame mTCUFrame=null;
    private String radarPeriod, radarHigh;
    private TextView infoTextView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_frame3);
        infoTextView = (TextView)findViewById(R.id.infoTextView);
        mTCUFrame = new TCUFrame("3","radarPeriod","radarHigh");
    }

    @Override
    protected void afterServiceConnected() {
        mZentriOSBLEService.setRequestID("3");
    }

    @Override
    protected void afterBroadcastReceive(String action, Intent intent) {
        switch (action){
            case ZentriOSBLEService.ACTION_STRING_DATA_READ:
                String buffer =ZentriOSBLEService.getData(intent);
                if(mTCUFrame.load(buffer)) {
                    radarPeriod = (String)mTCUFrame.get("radarPeriod");
                    radarHigh = (String)mTCUFrame.get("radarHigh") ;
                    infoTextView.setText("Radar signal period : \t" + radarPeriod + "ms\nHigh voltage in every period : \t" + radarHigh +"ms");
                }
                break;
        }
    }
}
