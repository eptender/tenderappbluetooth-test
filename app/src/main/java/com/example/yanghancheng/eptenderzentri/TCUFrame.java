package com.example.yanghancheng.eptenderzentri;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by YANG Hancheng on 13/05/2016.
 */
public class TCUFrame {

    private String frameID;
    private int length;
    private Map nameOrder;
    private Map orderValue;
    private String framePatternString;
    private Pattern mPattern;
    private Matcher mMatcher;
    private String frame="";

    public TCUFrame(String ID, String... keys){
        frameID = ID;
        int mOrder = 0;
        nameOrder = new HashMap();
        orderValue = new HashMap();
        framePatternString = ".*\\{(\\d+)";
        for(String name: keys){
            nameOrder.put(name, mOrder);
            orderValue.put(mOrder++, null);
            framePatternString = framePatternString + ":(\\-?\\w+)";
        }
        framePatternString = framePatternString + "\\}.*";
        mPattern = Pattern.compile(framePatternString);
        length = mOrder + 1;
    }

    public Object get(String name){
        return orderValue.get(nameOrder.get(name));
    }

    public boolean load(String frameFragment){
        mMatcher = mPattern.matcher(frame+frameFragment);
        synchronized (frame) {
            if (mMatcher.find()) {
                //frame found
                if(!mMatcher.group(1).equals(frameID)) {
                    frame="";
                    return false;
                }
                for(int i=1;i<length;i++){
                    orderValue.put(i-1,mMatcher.group(i+1));
                }
                frame = "";
                return true;
            } else {
                //if the frame is not found in buffer, continue buffering
                if (frame == "") {
                    frame = frameFragment;
                } else {
                    frame = frame + frameFragment;
                }
                return false;
            }
        }
    }
}