package com.example.yanghancheng.eptenderzentri;

import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.zentri.zentri_ble_command.ErrorCode;
import com.zentri.zentri_ble_command.ZentriOSBLEManager;

public class InitActivity extends BaseActivity{

    /** Request code to distinguish different **/
    private static final int BLE_ENABLE_REQ_CODE = 1;

    /** Activity lifecycle **/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_init);
        startService(new Intent(this, ZentriOSBLEService.class));
    }

    @Override
    protected void afterServiceConnected() {
        if(requirementsMet()){
            startScan();
        }
    }

    @Override
    protected void afterBroadcastReceive(String action, Intent intent) {
        switch (action){
            case ZentriOSBLEService.ACTION_SCAN_RESULT:
                //connect the 1st device whose name matches
                String name = ZentriOSBLEService.getData(intent);
                if(name!=null && name.startsWith(Const.DEVICE_NAME) && !mConnecting && !mConnected){
                    mZentriOSBLEManager.stopScan();
                    mZentriOSBLEManager.connect(name);
                    mConnecting = true;
                }
                break;
            case ZentriOSBLEService.ACTION_CONNECTED:
                mConnected = true;
                mZentriOSBLEManager.setMode(ZentriOSBLEManager.MODE_STREAM);
                Log.i(TAG, "Connected to " + ZentriOSBLEService.getData(intent));
                Intent nextActivityIntent = new Intent(this,MainActivity.class);
                startActivity(nextActivityIntent);
                break;
            case ZentriOSBLEService.ACTION_DISCONNECTED:
                mConnected = false;
                Log.i(TAG,"Disconnected");
                break;
            case ZentriOSBLEService.ACTION_ERROR:
                ErrorCode errorCode = ZentriOSBLEService.getErrorCode(intent);
                if (errorCode == ErrorCode.CONNECT_FAILED)
                {
                    mConnecting = false;//allow another attempt to connect
                    mConnected = false;
                    showErrorDialog(R.string.con_err_message, false);
                }
                Log.e(TAG,errorCode.name());
                break;
        }
    }

    /** Zentri function **/
    public void startScan(){
        if(mZentriOSBLEManager != null){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mZentriOSBLEManager.startScan();
                }
            });
        }
    }

    /** Activity with result **/
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == BLE_ENABLE_REQ_CODE){
            mZentriOSBLEService.initTruconnectManager();
            if(mZentriOSBLEManager.isInitialised()){
                //2nd try to initialise
                if(requirementsMet()){
                    startScan();
                } else {
                    // totally failed
                    showErrorDialog(R.string.init_fail_msg, true);
                }
            }
        }
    }

    /** Utility method **/
    private boolean requirementsMet(){
        boolean reqMet = false;
        if(!mZentriOSBLEManager.isInitialised()){
            //Enable BT
            startBLEEnableIntent();
        } else {
            reqMet = true;
        }
        return reqMet;
    }

    private void startBLEEnableIntent() {
        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivityForResult(enableBtIntent, BLE_ENABLE_REQ_CODE);
    }
}
