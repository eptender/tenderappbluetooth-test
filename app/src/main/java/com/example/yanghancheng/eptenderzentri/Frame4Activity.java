package com.example.yanghancheng.eptenderzentri;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class Frame4Activity extends BaseActivity {

    private TCUFrame mTCUFrame=null;
    private String totalMileage, totalFuel, currentMileage, currentFuelConsumption, currentSOCConsumption;
    private TextView infoTextView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_frame4);
        infoTextView = (TextView)findViewById(R.id.infoTextView);
        mTCUFrame = new TCUFrame("4","totalMileage", "totalFuel", "currentMileage", "currentFuelConsumption", "currentSOCConsumption");
    }

    @Override
    protected void afterServiceConnected() {
        mZentriOSBLEService.setRequestID("4");
    }

    @Override
    protected void afterBroadcastReceive(String action, Intent intent) {
        switch (action){
            case ZentriOSBLEService.ACTION_STRING_DATA_READ:
                String buffer =ZentriOSBLEService.getData(intent);
                if(mTCUFrame.load(buffer)) {
                    totalMileage = (String)mTCUFrame.get("totalMileage");
                    totalFuel = (String) mTCUFrame.get("totalFuel");
                    currentMileage = (String) mTCUFrame.get("currentMileage");
                    currentFuelConsumption = (String)mTCUFrame.get("currentFuelConsumption");
                    currentSOCConsumption = (String)mTCUFrame.get("currentSOCConsumption");
                    infoTextView.setText("Total mileage : \t" + totalMileage
                            +"\nTotal fuel consumption : \t"+ totalFuel
                            +"\nCurrent mileage : \t" + currentMileage
                            +"\nCurrent fuel consumption : \t" + currentFuelConsumption
                            +"\nCurrent energy consumption: \t" + currentSOCConsumption);
                }
                break;
        }
    }
}
