package com.example.yanghancheng.eptenderzentri;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.zentri.zentri_ble_command.ErrorCode;
import com.zentri.zentri_ble_command.ZentriOSBLEManager;

/**
 * Created by YANG Hancheng on 18/05/2016.
 */
public abstract class BaseActivity extends AppCompatActivity {

    /** Variables **/
    protected boolean mBound = false;
    protected boolean mConnected = false;
    protected boolean mConnecting = false;
    /** Activity components **/
    private Activity mActivity=this;
    protected LocalBroadcastManager mLocalBroadcastManager;
    protected ServiceConnection mServiceConnection;
    protected BroadcastReceiver mBroadcastReceiver;
    protected IntentFilter mIntentFilter;
    protected Handler mHandler;
    /** ZentriOSBLE components **/
    protected ZentriOSBLEService mZentriOSBLEService = null;
    protected ZentriOSBLEManager mZentriOSBLEManager = null;

    /** Debug **/
    protected String TAG = this.getClass().getSimpleName();


    /** Activity Lifecycle overrides **/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initBroadcastManager();
        initServiceConnection();
        initBroadcastReceiver();
        initReceiverIntentFilter();
        mHandler = new Handler();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mConnected = false;
        mConnecting = false;
        Intent intent = new Intent(this,ZentriOSBLEService.class);
        bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);
        mLocalBroadcastManager.registerReceiver(mBroadcastReceiver, mIntentFilter);
    }

    @Override
    protected void onStop() {
        if(mBound){
            mLocalBroadcastManager.unregisterReceiver(mBroadcastReceiver);
            unbindService(mServiceConnection);
            mBound = false;
            Log.d(TAG, "Unbind " + TAG + " with service");
        }
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        if(isTaskRoot()){
            stopService(new Intent(this, ZentriOSBLEService.class));
        }
        super.onDestroy();
    }

    /** Activity utility methods **/
    protected void initBroadcastManager(){
        mLocalBroadcastManager = LocalBroadcastManager.getInstance(getApplicationContext());
    }

    protected void initServiceConnection(){
        mServiceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                ZentriOSBLEService.LocalBinder mBinder = (ZentriOSBLEService.LocalBinder) service;
                mZentriOSBLEService = mBinder.getService();
                mBound = true;
                mZentriOSBLEManager = mZentriOSBLEService.getManager();
                afterServiceConnected();
                Log.d(TAG,"Service bind to "+ TAG);
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                mBound = false;
                Log.d(TAG,"Service unbind with" + TAG);
            }
        };
    }

    protected void initBroadcastReceiver(){
        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if(action == ZentriOSBLEService.ACTION_ERROR && ZentriOSBLEService.getErrorCode(intent)== ErrorCode.CONNECTION_LOST){
                    //reconnect
                    startReconnectActivity();
                } else {
                    afterBroadcastReceive(action, intent);
                }
            }
        };
    }

    protected void initReceiverIntentFilter(){
        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(ZentriOSBLEService.ACTION_SCAN_RESULT);
        mIntentFilter.addAction(ZentriOSBLEService.ACTION_CONNECTED);
        mIntentFilter.addAction(ZentriOSBLEService.ACTION_DISCONNECTED);
        mIntentFilter.addAction(ZentriOSBLEService.ACTION_ERROR);
        mIntentFilter.addAction(ZentriOSBLEService.ACTION_STRING_DATA_READ);
    }

    protected void startReconnectActivity(){
        Intent intent = new Intent(this, ReconnectActivity.class);
        startActivity(intent);
    }

    /** functions **/
    protected void showErrorDialog(final int msgID, final boolean finishOnClose){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Utility.showErrorDialog(mActivity, R.string.error, msgID, finishOnClose);
            }
        });
    }

    /** Activity abstract methods **/
    protected abstract void afterServiceConnected();

    protected abstract void afterBroadcastReceive(String action, Intent intent);

}
